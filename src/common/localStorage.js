export function getToken() {
  return localStorage.accessToken;
}

export function createToken(token) {
  localStorage.accessToken =token;
}

export function setStorePartnerToken(storePartnerData) {
  localStorage.storePartner = JSON.stringify(storePartnerData);
}

export function setUserInfo(userInfo) {
  localStorage.userInfo = JSON.stringify(userInfo);
}

export function getUserInfo() {
  const { userInfo } = localStorage;
  if (userInfo) {
    return JSON.parse(userInfo);
  }
  return null;
}

export function getStorePartnerToken() {
  const storePartnerData = localStorage.storePartner;
  if (storePartnerData) {
    return JSON.parse(storePartnerData);
  }
  return null;
}

export function destroyStorePartnerToken() {
  localStorage.removeItem('storePartner');
}

export function destroyToken() {
  localStorage.removeItem('accessToken');
}

export function destroyUserInfo() {
  localStorage.removeItem('userInfo');
}

export function getPerPageToken() {
  const storedPerPage = +localStorage.per_page;
  if (storedPerPage && Number.isInteger(storedPerPage)) {
    return storedPerPage;
  }
  return 15;
}

export function setPerPageToken(perPage) {
  localStorage.per_page = perPage;
}
