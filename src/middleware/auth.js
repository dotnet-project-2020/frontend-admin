// eslint-disable-next-line import/no-cycle
import store from '@/store';
import * as LocalStorage from '../common/localStorage';

export default async function auth({ to, next, router }) {
  if (!LocalStorage.getToken()) {
    const toPath = to.path;
    return router.push({
      name: 'login',
      query: { from: toPath }
    });
  }
  await store.dispatch('auth/fetchUserInfo');
  return next();
}
