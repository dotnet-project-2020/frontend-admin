import * as LocalStorage from '../common/localStorage';

export default function log({ next, router }) {
  if (LocalStorage.getToken()) {
    return router.push({ name: 'products' });
  }
  return next();
}
