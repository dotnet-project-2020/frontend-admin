import lodash from 'lodash';
import moment from 'moment-timezone';
import Select2 from 'v-select2-component';
import vi from '@/common/vi.json';
import translate from '@/mixins/localization';
import permission from '@/mixins/permission';
import titleMixin from '@/mixins/titleMixin';
import API from '@/services/axios';
import Vue from 'vue';
import CKEditor from '@ckeditor/ckeditor5-vue2';
import DefaultLayout from '@/layout/DefaultLayout.vue';
import SimpleLayout from '@/layout/SimpleLayout.vue';
import XLSX from 'xlsx';
import App from './App.vue';
import router from './router';
import store from './store';
import vuetify from './plugins/vuetify';

import './plugins';
import './assets/scss/main.scss';

window._ = lodash;
window.translations = vi;

Vue.mixin(translate);
Vue.mixin(permission);
Vue.mixin(titleMixin);

// window.__ = XLSX;
// layout
Vue.component('default-layout', DefaultLayout);
Vue.component('simple-layout', SimpleLayout);
Vue.component('select2', Select2);

Vue.config.productionTip = false;

Vue.use(XLSX);

Vue.use(CKEditor);

Vue.$store = store;
Vue.$router = router;
Vue.prototype.$axios = API;
Vue.prototype.$xlsx = XLSX;
Vue.prototype.moment = moment;

new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App),
}).$mount('#app');
