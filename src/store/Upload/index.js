export default {
  namespaced: true,
  state: {
    showFileChooserDialog: false
  },
  mutations: {
    SET_SHOW_FILE_CHOOSER_DIALOG(state, isShowFileChooserDialog) {
      state.showFileChooserDialog = isShowFileChooserDialog;
    }
  },
  actions: {
    setStateFileChooserDialog({ commit }, isShowFileChooserDialog) {
      commit('SET_SHOW_FILE_CHOOSER_DIALOG', isShowFileChooserDialog);
    }
  },

  getters: {
    showFileChooserDialog(state) {
      return state.showFileChooserDialog;
    }
  }
};
