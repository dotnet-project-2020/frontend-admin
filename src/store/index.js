import Vue from 'vue';
import Vuex from 'vuex';
// eslint-disable-next-line import/no-cycle
import auth from './Auth';
import notification from './Notification';
import upload from './Upload';
import partner from './Partner';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    auth,
    notification,
    upload,
    partner
  }
});
