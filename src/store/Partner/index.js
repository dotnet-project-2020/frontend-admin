// import * as PartnerAPI from '@/apis/partner';

export default {
  namespaced: true,
  state: {
    partners: []
  },
  mutations: {
    SET_PARTNERS(state, partners) {
      state.partners = partners;
    },
    ADD_PARTNER(state, thePartner) {
      state.partners.unshift(thePartner);
    },
    UPDATE_PARTNER(state, partnerUpdated) {
      const position = _.findIndex(state.partners, { id: partnerUpdated.id });
      state.partners.splice(position, 1, partnerUpdated);
    },
    REMOVE_PARTNER(state, thePartnerId) {
      state.partners = state.partners.filter((partner) => partner.id !== thePartnerId);
    }
  },
  actions: {
    // fetchPartners({ commit }, options) {
    //   PartnerAPI.list(options)
    //     .then(response => {
    //       commit('SET_PARTNERS', response.data.data);
    //     });
    // },
    setPartners({ commit }, partners) {
      commit('SET_PARTNERS', partners);
    },
    addPartner({ commit }, thePartner) {
      commit('ADD_PARTNER', thePartner);
    },
    updatePartner({ commit }, thePartner) {
      commit('UPDATE_PARTNER', thePartner);
    },
    removePartner({ commit }, thePartnerId) {
      commit('REMOVE_PARTNER', thePartnerId);
    }
  },

  getters: {
    getPartners(state) {
      return state.partners;
    },
    getPartner(state, id) {
      return state.partners.find(partner => partner.id === id);
    }
  }
};
