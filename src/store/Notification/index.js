export default {
  namespaced: true,
  state: {
    notifications: []
  },
  mutations: {
    SET_ALERT(state, notification) {
      state.notifications = state.notifications.concat(notification);
    },
    CLEAR_ALERT(state, notification) {
      if (state.notifications.length > 0) {
        state.notifications = state.notifications.filter((item) => item.key !== notification.key);
      }
    }
  },
  actions: {
    success({ commit }, notification) {
      const data = {};
      data.key = Math.floor(Math.random() * 10);
      data.message = notification.message;
      data.showing = notification.showing || true;
      data.color = notification.color || 'success';

      commit('SET_ALERT', data);

      setTimeout(() => {
        console.log('remove');
        commit('CLEAR_ALERT', data);
      }, 5000);
    },
    error({ commit }, notification) {
      const data = {};
      data.key = Math.floor(Math.random() * 10);
      data.message = notification.message;
      data.showing = notification.showing || true;
      data.color = notification.color || 'error';

      commit('SET_ALERT', data);

      setTimeout(() => {
        console.log('remove');
        commit('CLEAR_ALERT', data);
      }, 5000);
    },
    clear({ commit }) {
      commit('CLEAR_ALERT');
    }
  },

  getters: {
    notifications(state) {
      return state.notifications;
    }
  }
};
