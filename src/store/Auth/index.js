// eslint-disable-next-line import/no-cycle
import * as AuthAPI from '@/apis/auth';
import * as LocalStorage from '@/common/localStorage';

const userLocal = LocalStorage.getToken();
const userInfo = LocalStorage.getUserInfo();
const initialState = userLocal
  ? {
    loggedIn: true,
    user: userInfo
  }
  : {
    loggedIn: false,
    user: null
  };

export default {
  namespaced: true,

  state: initialState,

  mutations: {
    LOGIN_SUCCESS(state, user) {
      state.loggedIn = true;
      state.user = user;
    },
    LOGIN_FAILURE(state) {
      state.loggedIn = false;
      state.user = null;
    },
    LOGOUT(state) {
      state.loggedIn = false;
      state.user = null;
    },
    SET_USER_INFO(state, user) {
      state.user = user;
      LocalStorage.setUserInfo(user);
    }
  },

  actions: {
    loginSuccess({ commit }) {
      commit('LOGIN_SUCCESS');
    },
    loginFailure({ commit }) {
      commit('LOGIN_FAILURE');
    },
    setUserInfo({ commit }, user) {
      commit('SET_USER_INFO', user);
    },
    async fetchUserInfo({
      commit,
      state
    }) {
      if (!state.user) {
        await AuthAPI.userInfo()
          .then((res) => {
            commit('SET_USER_INFO', res.data);
          });
      }
    },
    logout({ commit }) {
      commit('LOGOUT');
    }
  },

  getters: {
    loggedIn(state) {
      return state.loggedIn;
    },
    currentUser(state) {
      return state.user;
    },
    isAdmin(state) {
      if (state.user && state.user.is_superadmin) {
        return state.user.is_superadmin;
      }
      return 0;
    }
  }
};
