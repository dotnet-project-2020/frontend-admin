import store from '@/store';
import Axios from '../services/axios';

const PATH = 'partners';

export function create(data) {
  const partners = store.getters['partner/getPartners'];
  return new Promise((resolve, reject) => {
    Axios.post(
      PATH,
      data
    )
      .then((res) => {
        const newPartner = res.data.data;
        // newPartner.allow_import = 1;
        if (partners && partners.length > 0) {
          store.dispatch('partner/addPartner', newPartner);
        }
        resolve(res.data.data);
      })
      .catch((err) => {
        reject(err);
      });
  });
}

export function update(data, id) {
  const partners = store.getters['partner/getPartners'];
  return new Promise((resolve, reject) => {
    Axios.put(
      `${PATH}/${id}`,
      data
    )
      .then((res) => {
        if (partners && partners.length > 0) {
          store.dispatch('partner/updatePartner', res.data.data);
        }
        resolve(res.data.data);
      })
      .catch((err) => {
        reject(err);
      });
  });
}

export function list() {
  const partners = store.getters['partner/getPartners'];
  return new Promise((resolve, reject) => {
    if (partners && partners.length > 0) {
      resolve(partners);
    } else {
      const partnerList = Axios.get(`${PATH}?perPage=10000`);
      partnerList
        .then((res) => {
          // store data
          store.dispatch('partner/setPartners', res.data.data);
          resolve(res.data.data);
        })
        .catch((err) => {
          reject(err);
        });
    }
  });
}

export function remove(id) {
  return new Promise((resolve, reject) => {
    Axios.delete(`${PATH}/${id}`)
      .then((res) => {
        store.dispatch('partner/removePartner', id);
        resolve(res.data?.data);
      })
      .catch((err) => {
        reject(err);
      });
  });
}

export function show(id) {
  const partners = store.getters['partner/getPartners'];
  const thePartner = _.find(partners, { id });
  return new Promise((resolve, reject) => {
    if (thePartner) {
      resolve(thePartner);
    } else {
      const showPartner = Axios.get(`${PATH}/${id}`);
      showPartner
        .then((res) => {
          resolve(res.data.data);
        })
        .catch((err) => {
          reject(err);
        });
    }
  });
}
