// eslint-disable-next-line import/no-cycle
import Axios from '../services/axios';

const PATH = 'account';

// export async function login(email, password) {
//   await Axios.get('../sanctum/csrf-cookie');
//   return Axios.post('login', {
//     username: email,
//     password
//   });
// }

export function logout() {
  return Axios.post('logout');
}

export function userInfo() {
  return Axios.get(PATH);
}

export function login(email, password) {
  return Axios.post(`${PATH}/login`, {
    email,
    password
  });
}

