export function createPaginationQuery(pagination) {
  const {
    itemsPerPage, sortBy, sortDesc, page
  } = pagination;
  let query = '?';
  // items per page
  if (itemsPerPage) {
    query += `perPage=${itemsPerPage}&`;
  }
  // what page want to get data
  if (page) query += `page=${page}&`;
  // order by column?

  if (sortBy && sortBy[0]) {
    query += `orderBy=${sortBy[0]}&`;
    // sort desc
    if (sortDesc && sortDesc[0]) {
      query += 'sortOrder=DESC&';
    } else {
      // sort asc
      query += 'sortOrder=ASC&';
    }
  }

  // split ? or &
  if (query === '?') query = '';
  if (query[query.length - 1] === '&') query = query.slice(0, -1);
  return query;
}

export function anotherFunction() { }
