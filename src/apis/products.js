import * as APIUtils from '@/apis/utils';
import Axios from '../services/axios';

const PATH = 'products';

export function list(options) {
  const query = APIUtils.createPaginationQuery(options);
  return Axios.get(PATH + query);
}

export function search(key) {
  return Axios.get(PATH, {
    params: {
      search: key
    }
  });
}
export function deleteProduct(id) {
  return Axios.delete(`${PATH}/${id}`);
}

export function show(id) {
  return Axios.get(`${PATH}/${id}`);
}

export function add(formData) {
  return Axios.post(`${PATH}`, formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  });
}

export function listTypes() {
  return Axios.get(`${PATH}/types/`);
}
export function listBrandss() {
  return Axios.get(`${PATH}/brands/`);
}
