import * as APIUtils from '@/apis/utils';
import Axios from '../services/axios';

const PATH = 'upload-file';

export function list(options) {
  const query = APIUtils.createPaginationQuery(options);
  return Axios.get(PATH + query);
}

export function show(id) {
  return Axios.get(
    `${PATH}/${id}`
  );
}

export function remove(id) {
  return Axios.delete(`${PATH}/${id}`);
}

export function upload(file, id) {
  const data = new FormData();
  data.append('partnerId', id);
  data.append('importFile', file);
  return Axios.post(
    PATH,
    data,
    {
      headers: { 'Content-Type': 'multipart/form-data' }
    }
  );
}

export function errorDetails(id, options) {
  const query = APIUtils.createPaginationQuery(options);
  return Axios.get(`${PATH}/${id}/errors${query}`);
}
