import * as APIUtils from '@/apis/utils';
import Axios from '../services/axios';

const PATH = 'contracts';

export function list(options) {
  const query = APIUtils.createPaginationQuery(options);
  return Axios.get(PATH + query);
}

export function exportFile(data) {
  return Axios.get(`${PATH}/export`, { params: data });
}

export function exportFileWithId(value) {
  return Axios.get(`${PATH}/export/${value}`);
}

export function exportFileFullWithId(value) {
  return Axios.get(`${PATH}/export-full/${value}`);
}

export function remove(id) {
  return Axios.delete(`${PATH}/${id}`);
}

export function show(id) {
  return Axios.get(`${PATH}/${id}`);
}
