import Axios from '../services/axios';

const PATH = 'general';

export function structure() {
  return Axios.get(`${PATH}/structure`);
}

export function types() {
  return Axios.get(`${PATH}/types`);
}
