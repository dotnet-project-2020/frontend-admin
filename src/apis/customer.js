import * as APIUtils from '@/apis/utils';
import Axios from '../services/axios';

const PATH = 'customers';

export function list(options) {
  const query = APIUtils.createPaginationQuery(options);
  return Axios.get(PATH + query);
}

export function remove(id) {
  return Axios.delete(`${PATH}/${id}`);
}

export function show(id) {
  return Axios.get(`${PATH}/${id}`);
}
