const array1 = [
  {
    column: 'code',
    header: 'APPL_ID',
    label: 'APPL_ID',
    require: 1
  },
  {
    column: 'birthday',
    header: 'DOB',
    label: 'DOB',
  },
  {
    column: 'nic_number',
    header: 'ID_NO',
    label: 'ID_NO',
    require: 1
  },
  {
    column: 'resident_number',
    header: 'FB_NO',
    label: 'FB_NO',
    require: 1
  }
];

function isTextInclude(text, _searchKey) {
  if (_searchKey) {
    const searchKey = _searchKey.split(' ');
    for (let i = 0; i < searchKey.length; i += 1) {
      if (text.toString()
        .includes(searchKey[i])) {
        return true;
      }
    }
    return false;
  }
  return true;
}

function isObjectInclude(item, search) {
  const arr = Object.values(item);
  if (arr.length > 0) {
    for (let i = 0; i < arr.length; i += 1) {
      if (isTextInclude(arr[i], search)) {
        return item;
      }
    }
  }
  return null;
}

function filters(arr, searchKey) {
  const resultFilters = [];
  arr.forEach(item => {
    const objectInclude = isObjectInclude(item, searchKey);
    if (objectInclude) {
      resultFilters.push(objectInclude);
    }
  });
  return resultFilters;
}

console.log(filters(array1, 'APP'));
