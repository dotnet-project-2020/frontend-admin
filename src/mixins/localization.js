export default {
  methods: {
    __(key, replace) {
      let translation = window.translations[key]
        ? window.translations[key] : key;

      _.forEach(replace, (value, key2) => {
        const searches = [
          `:${key2}`,
          `:${key2.toUpperCase()}`,
          `:${key2.charAt(0).toUpperCase()}${key2.slice(1)}`,
        ];

        const replacements = [
          value,
          value.toUpperCase(),
          value.charAt(0).toUpperCase() + value.slice(1),
        ];

        for (let i = searches.length - 1; i >= 0; i -= 1) {
          translation = translation.replace(searches[i], replacements[i]);
        }
      });
      return translation;
    },
  },
};
