import { mapGetters } from 'vuex';

export default {
  data() {
    return {
      user: {
        permissions: [
          // 'remove_uploads',
          'show_contracts',
          'show_customers',
          'store_partners',
          'show_partners',
          'update_partners',
          // 'remove_partners',
        ]
      }
    };
  },
  computed: {
    ...mapGetters({
      isAdmin: 'auth/isAdmin',
    })
  },
  mounted() {
  },

  methods: {
    hasPermission(permission) {
      if (this.isAdmin === 1) return true;
      if (this.user.permissions.includes(permission)) {
        return this.user.permissions.includes(permission);
      }
      return false;
    },

    hasPermissions(arrPermission) {
      for (let i = 0; i < arrPermission.length; i += 1) {
        if (!this.user.permissions.includes(arrPermission[i])) {
          return false;
        }
      }
      return true;
    },

    lastSegmentURL(path) {
      return path.substring(path.lastIndexOf('/') + 1);
    }
  },
};
