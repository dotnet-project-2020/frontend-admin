// Automatically loads and bootstraps files
// in the "./src/components/Base" folder.

// Imports
import Vue from 'vue';

// Base component
import Button from '@/components/Base/Button.vue';
import ShowButton from '@/components/Base/ShowButton.vue';
import DownloadButton from '@/components/Base/DownloadButton.vue';
import UpdateButton from '@/components/Base/UpdateButton.vue';
import DuplicateButton from '@/components/Base/DuplicateButton.vue';
import RemoveButton from '@/components/Base/RemoveButton.vue';
import Checkbox from '@/components/Base/Checkbox.vue';
import TextField from '@/components/Base/TextField.vue';
import Autocomplete from '@/components/Base/Autocomplete.vue';
import Select from '@/components/Base/Select.vue';
import Card from '@/components/Base/Card.vue';
import Menu from '@/components/Base/Menu.vue';
import Alert from '@/components/Base/Alert.vue';
import Dropdown from '@/components/Base/Dropdown.vue';
import List from '@/components/Base/List.vue';
import ProgressLinear from '@/components/Base/ProgressLinear.vue';
import Radio from '@/components/Base/Radio.vue';
import Textarea from '@/components/Base/Textarea.vue';
import DataTable from '@/components/Base/Table.vue';
import Tooltip from '@/components/Base/Tooltip.vue';
import Skeleton from '@/components/Base/Skeleton.vue';
import FileInput from '@/components/Base/FileInput.vue';
import ComponentLoading from '@/components/Base/ComponentLoading.vue';
import Dialog from '@/components/Base/Dialog.vue';

// Simple component
import SimpleTextField from '@/components/Base/SimpleTextField.vue';
import SimpleSelect from '@/components/Base/SimpleSelect.vue';
import SimpleCheckbox from '@/components/Base/SimpleCheckbox.vue';

// Select 2
import SelectTwo from '@/components/Base/SelectTwo.vue';

Vue.component('base-btn', Button);
Vue.component('show-btn', ShowButton);
Vue.component('update-btn', UpdateButton);
Vue.component('download-btn', DownloadButton);
Vue.component('duplicate-btn', DuplicateButton);
Vue.component('remove-btn', RemoveButton);
Vue.component('base-checkbox', Checkbox);
Vue.component('base-text-field', TextField);
Vue.component('base-autocomplete', Autocomplete);
Vue.component('base-select', Select);
Vue.component('base-card', Card);
Vue.component('base-menu', Menu);
Vue.component('base-alert', Alert);
Vue.component('base-dropdown', Dropdown);
Vue.component('base-list', List);
Vue.component('base-progress-linear', ProgressLinear);
Vue.component('base-radio', Radio);
Vue.component('base-textarea', Textarea);
Vue.component('base-data-table', DataTable);
Vue.component('base-tooltip', Tooltip);
Vue.component('base-skeleton-loader', Skeleton);
Vue.component('base-file-input', FileInput);
Vue.component('base-component-loading', ComponentLoading);
Vue.component('base-dialog', Dialog);

// Simple component
Vue.component('simple-text-field', SimpleTextField);
Vue.component('simple-select', SimpleSelect);
Vue.component('simple-checkbox', SimpleCheckbox);

// Select 2
Vue.component('select-two', SelectTwo);
