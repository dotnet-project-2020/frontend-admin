import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import '@mdi/font/css/materialdesignicons.css';

Vue.use(Vuetify);
const opts = {};
export default new Vuetify({
  opts,
  icons: {
    iconfont: 'mdi',
  },
  theme: {
    dark: false,
    themes: {
      light: {
        primary: '#248afd',
        secondary: '#a3a4a5',
        success: '#71c016',
        warning: '#f5a623',
        accent: '#68afff',
        error: '#ff4747',
      },
      dark: {},
    },
  },
});
