// Automatically included in './src/main.js'
// base components
import './base';
// vee-validate
import './vee-validate';
// vue scroll to
import './vue-scrollto';
