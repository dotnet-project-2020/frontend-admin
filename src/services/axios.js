import axios from 'axios';
// eslint-disable-next-line import/no-cycle
import router from '@/router';
import Vue from 'vue';
import * as LocalStorage from '@/common/localStorage';
import { getToken } from '../common/localStorage';

const Axios = axios.create({
  baseURL: 'https://thegioididongfake.com/v1',
  withCredentials: true
});

Axios.interceptors.request.use(config => {
  const token = getToken();
  if (token) {
    config.headers.common['Authorization'] = 'Bearer ' + token;
  }
  return config;
});

// Axios.defaults.timeout = 60000;

Axios.interceptors.response.use(
  response => Promise.resolve(response),
  error => {
    if (error.response?.status) {
      switch (+error.response.status) {
        case 400:
          // do something
          break;
        case 401: {
          LocalStorage.destroyToken();
          Vue.$router.push({ name: 'login' });
          break;
        }
        case 403:
          // Vue.$router.replace({
          //   path: '/login',
          //   query: { redirect: router.currentRoute.fullPath }
          // });
          break;
        case 404:
          Vue.$router.push({ name: '404' });
          break;
        case 502:
          setTimeout(() => {
            Vue.$router.replace({
              path: '/login',
              query: {
                redirect: router.currentRoute.fullPath
              }
            });
          }, 1000);
          break;
        default:
          break;
      }
    }
    return Promise.reject(error.response);
  }
);

export default Axios;
