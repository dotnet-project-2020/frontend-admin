import * as Constants from '@/common/Constants/Router';

export default [
  {
    path: '/admin/products',
    name: Constants.PRODUCTS,
    component: () => import('@/views/Admin/Products/Index.vue'),
    meta: { layout: 'simple' }
  },
  {
    path: '/admin/products/new',
    name: Constants.PRODUCTS_ADD,
    component: () => import('@/views/Admin/Products/New/Index.vue'),
    meta: { layout: 'simple' }
  }
];
