import Vue from 'vue';
import VueRouter from 'vue-router';

// eslint-disable-next-line import/no-cycle
import auth from '../middleware/auth';
import log from '../middleware/log';
import routerProduct from './product';

Vue.use(VueRouter);

const routes = [
  ...routerProduct,
  {
    path: '*',
    name: '404',
    component: () => import('@/views/404/Index.vue'),
    meta: { layout: 'simple' }
  },
  {
    path: '/',
    redirect: { name: 'login' },
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('@/views/Login/Index.vue'),
    meta: {
      layout: 'simple',
      middleware: [log],
    }
  },
  {
    path: '/dashboard',
    name: 'admin',
    component: () => import('@/views/Admin/Index.vue'),
    meta: {
      middleware: [auth],
    },
  },
  {
    path: '/admin/ui',
    name: 'ui',
    component: () => import('@/views/UI/Index.vue'),
    meta: {
      middleware: [auth],
    },
  },
  {
    path: '/admin/user',
    name: 'user',
    component: () => import('@/views/Admin/User/Index.vue'),
    meta: {
      middleware: [auth],
    },
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

function nextFactory(context, middleware, index) {
  const subsequentMiddleware = middleware[index];
  // If no subsequent Middleware exists,
  // the default `next()` callback is returned.
  if (!subsequentMiddleware) return context.next;

  return (...parameters) => {
    // Run the default Vue Router `next()` callback first.
    context.next(...parameters);
    // Then run the subsequent Middleware with a new
    // `nextMiddleware()` callback.
    const nextMiddleware = nextFactory(context, middleware, index + 1);
    subsequentMiddleware({
      ...context,
      next: nextMiddleware
    });
  };
}

router.beforeEach((to, from, next) => {
  if (to.meta.middleware) {
    const middleware = Array.isArray(to.meta.middleware)
      ? to.meta.middleware
      : [to.meta.middleware];

    const context = {
      from,
      next,
      router,
      to,
    };
    const nextMiddleware = nextFactory(context, middleware, 1);

    return middleware[0]({
      ...context,
      next: nextMiddleware
    });
  }

  return next();
});

export default router;
