module.exports = {
  transpileDependencies: [
    'vuetify',
  ],
  devServer: {
    host: 'thegioididongfake.com',
    port: '2000',
    https: true
  },
};
